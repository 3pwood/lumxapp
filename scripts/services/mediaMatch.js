angular
.module('lumapp')
.service('mediaMatch', mediaMatch);

function mediaMatch() {
    let media = {};

    function getMedia() {
        /** RULES **/
        media.rules = {
            phone: "(max-width: 767px)",
            tablet: "(min-width: 768px) and (max-width: 979px)",
            desktop: "(min-width: 980px)"
        };

        /** MATCHES **/
        media.matches = {
            phone: window.matchMedia(media.rules.phone),
            tablet: window.matchMedia(media.rules.tablet),
            desktop: window.matchMedia(media.rules.desktop)
        };
    }
    getMedia();

    return {
        media: media
    }
}