// Declare app level module which depends on views, and components
angular.module('lumapp', [
    'ui.router',
    'lumx'
])
.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
    /** HOME STATES **/
    .state('home', {
        url: '/home',
        templateUrl: 'views/home.html'
    })
    .state('playground', {
        url: '/playground',
        templateUrl: 'views/playground.html'
    })
    // nested list with just some random string data
    .state('playground.paragraph', {
        url: '/show',
        template: '<div>I could sure use a drink right now.</div>'
    })
})
.run(function($rootScope, $location, $timeout, mediaMatch) {
    $rootScope.media = {};
    $rootScope.media.isDesktop = mediaMatch.media.matches.desktop.matches;
    $rootScope.media.isTablet = mediaMatch.media.matches.tablet.matches;
    $rootScope.media.isPhone = mediaMatch.media.matches.phone.matches;

    window.onresize = function () {
        // this.console.log('media', $rootScope.media);
        $rootScope.media.isDesktop = mediaMatch.media.matches.desktop.matches;
        $rootScope.media.isTablet = mediaMatch.media.matches.tablet.matches;
        $rootScope.media.isPhone = mediaMatch.media.matches.phone.matches;
        $rootScope.$apply();
    }
});